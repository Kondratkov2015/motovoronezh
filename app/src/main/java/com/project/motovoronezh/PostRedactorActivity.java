package com.project.motovoronezh;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.requests.UploadImageRequest;
import com.project.motovoronezh.data.services.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PostRedactorActivity extends AppCompatActivity {

    private ImageButton backBtn;
    private ImageButton photoBtn;
    private ImageButton textBtn;
    private ImageButton uploadBtn;

    private ScrollView scrollView;
    private ProgressBar loading;

    private LinearLayout layout;

    private List<String> imageURLs = new ArrayList<>();

    private String postMessage = "";

    private int counter = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_redactor);

        backBtn = findViewById(R.id.back_btn);
        photoBtn = findViewById(R.id.photo_btn);
        textBtn = findViewById(R.id.text_btn);
        uploadBtn = findViewById(R.id.upload_btn);

        scrollView = findViewById(R.id.post_redactor_scrollView);
        loading = findViewById(R.id.post_redactor_loading);

        layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        scrollView.setPadding(0, 20, 0, 0);
        scrollView.addView(layout);

        EditText titleTextView = new EditText(getApplicationContext());
        titleTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        titleTextView.setHint("Введите название поста");
        titleTextView.setTypeface(null, Typeface.BOLD);
        titleTextView.setPadding(20, 30, 20, 30);
        titleTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        titleTextView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.white));
        titleTextView.setFocusable(true);

        layout.addView(titleTextView);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostRedactorActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        textBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText textBlock = new EditText(getApplicationContext());
                textBlock.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                textBlock.setHint("Введите текст\n\n");
                textBlock.setPadding(20, 30, 20, 30);
                textBlock.setGravity(Gravity.START);
                textBlock.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.white));
                attachDeleteDialog(textBlock);
                textBlock.setFocusable(true);

                layout.addView(textBlock);
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                textBlock.requestFocus();

            }
        });

        photoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading.setVisibility(View.VISIBLE);
                try {
                    new uploadImages().execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    class uploadImages extends AsyncTask<LinearLayout, Void, Boolean> {

        @Override
        protected Boolean doInBackground(LinearLayout... layouts) {
            try {
                for (int i = counter; i < layout.getChildCount(); i++) {
                    View view = layout.getChildAt(i);

                    if (view instanceof EditText) {
                        postMessage = postMessage.concat(((EditText) view).getText().toString() + "\n");
                    }

                    if (view instanceof ImageView) {
                        BitmapDrawable drawable = (BitmapDrawable) ((ImageView) view).getDrawable();
                        Bitmap bitmap = drawable.getBitmap();
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
                        byte[] bitmapData = bos.toByteArray();
                        String currentDate = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
                        File f = new File(getCacheDir(), "temp_" + currentDate);
                        f.createNewFile();
                        FileOutputStream fos = new FileOutputStream(f);
                        fos.write(bitmapData);
                        fos.flush();
                        fos.close();

                        File fCopy = new File(getCacheDir(), "temp_" + currentDate + "_copy");
                        fCopy.createNewFile();
                        copy(f, fCopy);

                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fCopy);
                        MultipartBody.Part body = MultipartBody.Part.createFormData("image", f.getName(), requestFile);

                        UploadImageRequest request = new UploadImageRequest();

                        int finalI = i;
                        request.uploadImage(body, new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                try {
                                    imageURLs.add(request.getPhotoURL());
                                    postMessage = postMessage.concat("[img]" + request.getPhotoURL() + "[/img]" + "\n");
                                    if (finalI != layout.getChildCount() - 1) {
                                        counter = finalI + 1;
                                        new uploadImages().execute();
                                    }
                                } catch (Exception e) {
                                    //todo алерт и идти дальше, можно через диалог
                                    System.out.println("ОШИБКА В РЕКВЕСТЕ ЗАГРУЗКИ ФОТО");
                                    e.printStackTrace();
                                    if (finalI != layout.getChildCount() - 1) {
                                        counter = finalI + 1;
                                        new uploadImages().execute();
                                    }
                                }
                                return null;
                            }
                        });
                        if (i == layout.getChildCount() - 1) return true;
                        break;
                    }
                    if (i == layout.getChildCount() - 1) return true;
                }
                return false; //todo
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean flag) {
            //imageURLs.addAll(urls); //todo обработать налл
            if (flag) {
                loading.setVisibility(View.GONE);
                System.out.println("POST EXECUTE");
                System.out.println(postMessage);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            try {
                Uri selectedImage = data.getData();
                Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
                ImageView imageBlock = new ImageView(getApplicationContext());
                imageBlock.setAdjustViewBounds(true);
                imageBlock.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                int width = metrics.widthPixels / 2;

                imageBlock.setLayoutParams(new LinearLayout.LayoutParams(width,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                imageBlock.setImageBitmap(bitmap);
                attachDeleteDialog(imageBlock);
                imageBlock.setPadding(5, 5, 5, 5);

                layout.addView(imageBlock);

                scrollView.fullScroll(ScrollView.FOCUS_DOWN);

            } catch (Exception e) {
                System.out.println("TROUBLE");
                e.printStackTrace();
            }
        }

    }

    private void attachDeleteDialog(View view) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Удалить выбранный блок?")
                .setCancelable(true)
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        try {
                            layout.removeView(view);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        dialog = builder.create();

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                dialog.show();
                return false;
            }
        });
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}