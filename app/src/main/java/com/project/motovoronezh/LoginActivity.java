package com.project.motovoronezh;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.UserData;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.requests.UserRequests;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

public class LoginActivity extends AppCompatActivity {

    private final int DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernameEditText = findViewById(R.id.login_username);
        final EditText passwordEditText = findViewById(R.id.login_password);

        final Button loginButton = findViewById(R.id.login_loginBtn);
        final Button registrButton = findViewById(R.id.login_registsrBtn);



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);*/
                UserConfig.clearUserInfo(getApplicationContext());
                UserRequests.login(usernameEditText.getText().toString(), passwordEditText.getText().toString(), getApplicationContext(), new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                new Timer().schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    }
                                }, DELAY);
                                //UserData.getInstance().requestUserData(UserConfig.getToken(getApplicationContext()), UserConfig.getUserId(getApplicationContext()));
                                return null;
                            }
                        },
                        new Callable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
                                Log.e("LOGIN FAILED", "check password");
                                return null;
                            }
                        });


                return;
            }
        });

        registrButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://motovoronezh.ru/forum/reg.php"));
                startActivity(browserIntent);
            }
        });

    }


}
