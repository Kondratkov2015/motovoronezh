package com.project.motovoronezh.ui.dalnoboy;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.project.motovoronezh.R;
import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.dto.ShortPost;
import com.project.motovoronezh.data.dto.User;
import com.project.motovoronezh.data.requests.AllDalnoboyRequest;
import com.project.motovoronezh.data.requests.AllNewsRequest;
import com.project.motovoronezh.data.requests.PostRequests;
import com.project.motovoronezh.data.services.Utils;
import com.project.motovoronezh.ui.news.NewsTimelineFragment;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.project.motovoronezh.data.services.Utils.convertDateToHumanReadable;
import static com.project.motovoronezh.data.services.Utils.findAllImageURLFromText;
import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;

public class DalnoboyPostFragment extends Fragment {

    public static final String ARGUMENT_POST_ID = "postId";
    public static final String ARGUMENT_TITLE = "title";
    public static final String ARGUMENT_POSTER = "poster";
    public static final String ARGUMENT_DATE = "date";

    private ScrollView scrollView;

    private Post post;
    private int post_id;
    private String title;
    private String poster;
    private long date;
    private LinearLayout layout;
    private ProgressBar loading;

    private String postMessage;

    private int updateTimelineCounter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dalnoboy_post, container, false);
        scrollView = root.findViewById(R.id.dalnoboy_post_scrollView);
        loading = root.findViewById(R.id.dalnoboy_post_loading);
        layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.CENTER);
        scrollView.addView(layout);

        post_id = (int) getArguments().get(ARGUMENT_POST_ID);
        title = getArguments().get(ARGUMENT_TITLE).toString();
        poster = getArguments().get(ARGUMENT_POSTER).toString();
        date = (long) getArguments().get(ARGUMENT_DATE);

        postRequest(post_id);

        return root;
    }

    private void postRequest(int postId) {
        PostRequests request = new PostRequests();
        request.getPostMessage(UserConfig.getToken(getActivity().getApplicationContext()), postId, new Callable<Void>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public Void call() throws Exception {
                loading.setVisibility(View.VISIBLE);
                postMessage = request.getMessage();
                new parsePostFromMessage().execute();
                return null;
            }
        });
    }

    class parsePostFromMessage extends AsyncTask<String, Void, List<List<Drawable>>> { //todo check work

        @Override
        protected List<List<Drawable>> doInBackground(String... strings) {
            List<List<Drawable>> drawables = new ArrayList<>();
            try {
                drawables.add(new ArrayList<>());
                List<String> URLs = findAllImageURLFromText(postMessage);
                if (URLs != null && !URLs.isEmpty()) {
                    for (int j = 0; j < URLs.size(); j++) {
                        try {
                            drawables.get(0).add(loadImageFromWebOperations(URLs.get(j)));
                            //break;
                        } catch (Exception e) {
                            //drawables.get(i).add(null); // null нужен для понимания, в какое место текста вставлять картинку
                            //e.printStackTrace();
                        }
                    }
                }
                return drawables;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<List<Drawable>> images) {
            try {
                Post post = new Post(post_id, title, postMessage, new User(poster), date);
                LinearLayout postLayout = Utils.createFullPostInfoLayout(post.getTitle(), post.getMessage(), 0, images, getActivity().getApplicationContext());
                layout.addView(postLayout);

                loading.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                loading.setVisibility(View.GONE);
                // todo выводить алерт
            }
        }
    }


}