package com.project.motovoronezh.ui.dalnoboy;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DalnoboyViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DalnoboyViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}