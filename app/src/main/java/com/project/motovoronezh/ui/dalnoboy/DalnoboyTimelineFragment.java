package com.project.motovoronezh.ui.dalnoboy;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.motovoronezh.R;
import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.dto.ShortPost;
import com.project.motovoronezh.data.requests.AllDalnoboyRequest;
import com.project.motovoronezh.data.services.Utils;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.project.motovoronezh.data.services.Utils.createShortPostInfoLayout;
import static com.project.motovoronezh.data.services.Utils.deleteAllTagsFromText;
import static com.project.motovoronezh.data.services.Utils.findImageURLFromText;
import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;

public class DalnoboyTimelineFragment extends Fragment {

    private DalnoboyViewModel dalnoboyViewModel;
    private ScrollView scrollView;

    private List<ShortPost> dalnoboysPosts;
    private LinearLayout layout;
    private TextView updateTimelineTextView;
    private ProgressBar loading;

    private DalnoboyPostFragment dalnoboyPostFragment;

    private int updateTimelineCounter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dalnoboyViewModel =
                new ViewModelProvider(this).get(DalnoboyViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dalnoboy_timeline, container, false);
        scrollView = root.findViewById(R.id.dalnoboy_timeline_scrollView);
        loading = root.findViewById(R.id.dalnoboy_timeline_loading);
        layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.CENTER);
        scrollView.addView(layout);

        dalnoboysPosts = new ArrayList<>();
        dalnoboysRequest(0, root);

        return root;
    }


    private void dalnoboysRequest(int fromIndex, View root) {
        AllDalnoboyRequest request = new AllDalnoboyRequest();
        request.getAllDalnoboy(UserConfig.getToken(getActivity().getApplicationContext()), fromIndex, new Callable<Void>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public Void call() throws Exception {
                loading.setVisibility(View.VISIBLE);
                dalnoboysPosts.addAll(request.getDalnoboysPostList());
                updateTimelineCounter++;
                try {
                    int dalnoboysPostsSize = dalnoboysPosts.size();
                    int start = dalnoboysPostsSize % 20 == 0 ? dalnoboysPostsSize - 20 : dalnoboysPostsSize - dalnoboysPostsSize % 20;
                    for (int i = start; i < dalnoboysPostsSize; i++) {
                        LinearLayout postLayout = Utils.createShortPostInfoLayout(dalnoboysPosts, i, getContext());
                        layout.addView(postLayout);
                        View delimiter = new View(getActivity().getApplicationContext());
                        delimiter.setBackground(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.text_view_border));
                        delimiter.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                21));
                        layout.addView(delimiter);
                        int finalI = i;
                        postLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dalnoboyPostFragment = new DalnoboyPostFragment();
                                Bundle bundle = new Bundle();
                                bundle.putInt(DalnoboyPostFragment.ARGUMENT_POST_ID, dalnoboysPosts.get(finalI).getPostId());
                                bundle.putString(DalnoboyPostFragment.ARGUMENT_TITLE, dalnoboysPosts.get(finalI).getTitle());
                                bundle.putString(DalnoboyPostFragment.ARGUMENT_POSTER, dalnoboysPosts.get(finalI).getUser().getUsername());
                                bundle.putLong(DalnoboyPostFragment.ARGUMENT_DATE, dalnoboysPosts.get(finalI).getData());
                                dalnoboyPostFragment.setArguments(bundle);
                                root.setEnabled(false);
                                root.setVisibility(View.INVISIBLE);
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, dalnoboyPostFragment).commit();
                            }
                        });
                    }

                    layout.removeView(updateTimelineTextView);

                    updateTimelineTextView = new TextView(getContext());
                    updateTimelineTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            100));
                    updateTimelineTextView.setGravity(Gravity.CENTER);
                    //updateTimelineTextView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
                    updateTimelineTextView.setText("Далее");
                    updateTimelineTextView.setEnabled(true);
                    updateTimelineTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dalnoboysRequest(updateTimelineCounter * 20, root);
                            updateTimelineTextView.setEnabled(false);
                        }
                    });
                    layout.addView(updateTimelineTextView);
                    loading.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                    loading.setVisibility(View.GONE);
                    // todo выводить алерт
                }
                return null;
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().getSupportFragmentManager().beginTransaction().remove(dalnoboyPostFragment).commit();
        } catch (Exception e) {}
    }

}