package com.project.motovoronezh.ui.events;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.project.motovoronezh.R;
import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.dto.Event;
import com.project.motovoronezh.data.requests.AllEventsRequest;
import com.project.motovoronezh.data.services.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.project.motovoronezh.data.services.Utils.findAllImageURLFromText;
import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;


public class EventsTimelineFragment extends Fragment {

    private ScrollView scrollView;

    private List<Event> events;
    private LinearLayout layout;
    private ProgressBar loading;

    private UpdateEventsTimeline updateEventsTimeline;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_events_timeline, container, false);
        scrollView = root.findViewById(R.id.events_timeline_scrollView);
        loading = root.findViewById(R.id.events_timeline_loading);
        layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.CENTER);
        scrollView.addView(layout);
        updateEventsTimeline = new UpdateEventsTimeline();

        events = new ArrayList<>();
        eventsRequest();

        return root;
    }

    private void eventsRequest() {
        AllEventsRequest request = new AllEventsRequest();
        request.getAllEvents(UserConfig.getToken(getActivity().getApplicationContext()), new Callable<Void>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public Void call() throws Exception {
                loading.setVisibility(View.VISIBLE);
                events.addAll(request.getEventsList());
                updateEventsTimeline.execute();
                return null;
            }
        });
    }

    class UpdateEventsTimeline extends AsyncTask<String, Void, List<List<Drawable>>> {

        @Override
        protected List<List<Drawable>> doInBackground(String... strings) {
            try {
                List<List<Drawable>> drawables= new ArrayList<>();
                int eventsPostsSize = events.size();
                for (int i = 0; i < eventsPostsSize; i++) {
                    drawables.add(new ArrayList<>());
                    int drawableIndex = i % 20;
                    List<String> URLs = findAllImageURLFromText(events.get(i).getMessage());
                    if (URLs != null) {
                        for (int j = 0; j < URLs.size(); j++) {
                            try {
                                drawables.get(drawableIndex).add(loadImageFromWebOperations(URLs.get(j)));
                                //break;
                            } catch (Exception e) {
                                //drawables.get(i).add(null); // null нужен для понимания, в какое место текста вставлять картинку
                                //e.printStackTrace();
                            }
                        }
                    }
                }
                return drawables;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<List<Drawable>> images) {
            try {
                for (int i = 0; i < events.size(); i++) {
                    LinearLayout eventLayout = Utils.createFullPostInfoLayout(events.get(i).getTitle(), events.get(i).getMessage(), i, images, getActivity().getApplicationContext());
                    //eventLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
                    layout.addView(eventLayout);
                    View delimiter = new View(getActivity().getApplicationContext());
                    delimiter.setBackground(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.text_view_border));
                    delimiter.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            21));
                    layout.addView(delimiter);
                }

                loading.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                loading.setVisibility(View.GONE);
                // todo выводить алерт
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        updateEventsTimeline.cancel(true);
    }

}
