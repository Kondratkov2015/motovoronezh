package com.project.motovoronezh.ui.news;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.project.motovoronezh.R;
import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.requests.AllNewsRequest;
import com.project.motovoronezh.data.services.Utils;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.project.motovoronezh.data.services.Utils.findAllImageURLFromText;
import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;

public class NewsTimelineFragment extends Fragment {

    private NewsViewModel newsViewModel;
    private ScrollView scrollView;

    private List<Post> posts;
    private LinearLayout layout;
    private TextView updateTimelineTextView;
    private ProgressBar loading;

    private UpdateNewsTimeline updateTimeline;

    private int updateTimelineCounter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        newsViewModel =
                new ViewModelProvider(this).get(NewsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_news_timeline, container, false);
        scrollView = root.findViewById(R.id.news_timeline_scrollView);
        loading = root.findViewById(R.id.news_timeline_loading);
        layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setGravity(Gravity.CENTER);
        scrollView.addView(layout);

        updateTimeline = new UpdateNewsTimeline();
        posts = new ArrayList<>();
        newsRequest(0);


        return root;
    }


    private void newsRequest(int fromIndex) {
        AllNewsRequest request = new AllNewsRequest();
        request.getAllNews(UserConfig.getToken(getActivity().getApplicationContext()), fromIndex, new Callable<Void>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public Void call() throws Exception {
                loading.setVisibility(View.VISIBLE);
                posts.addAll(request.getPostList());
                //new updateNewsTimeline().execute();
                updateTimeline.execute();
                updateTimelineCounter++;
                return null;
            }
        });
    }


    class UpdateNewsTimeline extends AsyncTask<String, Void, List<List<Drawable>>> {

        @Override
        protected List<List<Drawable>> doInBackground(String... strings) {
            try {
                List<List<Drawable>> drawables= new ArrayList<>();
                int newsPostsSize = posts.size();
                int start = newsPostsSize % 20 == 0 ? newsPostsSize - 20 : newsPostsSize - newsPostsSize % 20;
                for (int i = start; i < newsPostsSize; i++) {
                    drawables.add(new ArrayList<>());
                    int drawableIndex = i % 20;
                    List<String> URLs = findAllImageURLFromText(posts.get(i).getMessage());
                    if (URLs != null) {
                    for (int j = 0; j < URLs.size(); j++) {
                        try {
                            drawables.get(drawableIndex).add(loadImageFromWebOperations(URLs.get(j)));
                            //break;
                        } catch (Exception e) {
                            //drawables.get(i).add(null); // null нужен для понимания, в какое место текста вставлять картинку
                            //e.printStackTrace();
                        }
                    }
                    }
                }
                return drawables;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<List<Drawable>> images) {
            try {
                int newsPostsSize = posts.size();
                int start = newsPostsSize % 20 == 0 ? newsPostsSize - 20 : newsPostsSize - newsPostsSize % 20;
                for (int i = start; i < newsPostsSize; i++) {
                    LinearLayout postLayout = Utils.createFullPostInfoLayout(posts.get(i).getTitle(), posts.get(i).getMessage(), i, images, getActivity().getApplicationContext());
                    //postLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
                    layout.addView(postLayout);
                    View delimiter = new View(getActivity().getApplicationContext());
                    delimiter.setBackground(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.text_view_border));
                    delimiter.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            21));
                    layout.addView(delimiter);
                }

                layout.removeView(updateTimelineTextView);

                updateTimelineTextView = new TextView(getContext());
                updateTimelineTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        100));
                updateTimelineTextView.setGravity(Gravity.CENTER);
                //updateTimelineTextView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.text_view_border));
                updateTimelineTextView.setText("Далее");
                updateTimelineTextView.setEnabled(true);
                updateTimelineTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newsRequest(updateTimelineCounter * 20);
                        updateTimelineTextView.setEnabled(false);
                    }
                });
                layout.addView(updateTimelineTextView);
                loading.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                loading.setVisibility(View.GONE);
                // todo выводить алерт
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        updateTimeline.cancel(true);
    }
}