package com.project.motovoronezh;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.UserData;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import okhttp3.Request;

import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;

public class TransitionActivity extends AppCompatActivity {


    private final int DELAY = 3000;

    private ImageView transitionImage;

    private final String LOAD_IMAGE_URL = "http://motovoronezh.ru/ix/logo_top.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition);

        transitionImage = findViewById(R.id.transition_image);


        new AsyncRequest().execute(LOAD_IMAGE_URL);

        String token = UserConfig.getToken(getApplicationContext());

        if (token != null && !token.isEmpty()) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    goToNextActivity(true);
                }
            }, DELAY);

            UserData.getInstance().requestUserData(token, UserConfig.getUserId(getApplicationContext()));

        } else {
            goToNextActivity(false);
        }
    }


    private void goToNextActivity(boolean hasToken) {
        Intent intent = hasToken ? new Intent(TransitionActivity.this, MainActivity.class)
                : new Intent(TransitionActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    class AsyncRequest extends AsyncTask<String, Void, Drawable> {

        @Override
        protected Drawable doInBackground(String... urls) {
            try {
                return loadImageFromWebOperations(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable image) { //todo not get image case
            try {
                transitionImage.setImageDrawable(image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}