package com.project.motovoronezh.data;

import com.project.motovoronezh.data.dto.User;

public class UserData {

    private static volatile UserData userData;

    private User user;


    public static UserData getInstance() {
        if(userData == null)
            synchronized (UserData.class) {
                if(userData == null) {
                    userData = new UserData();
                }
            }
        return userData;
    }

    public void initUser(User user) {
        this.user = user;
    }

    public void requestUserData(String token, String userId) {

        //UserRequests.getUser(token, userId);
    }

    public User getUser() {
        return user;
    }

}
