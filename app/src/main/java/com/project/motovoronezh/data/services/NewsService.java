package com.project.motovoronezh.data.services;

import com.project.motovoronezh.data.dto.Post;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NewsService {

    @GET("get_all_news/{fromIndex}")
    Call<Post[]> getAllNews(@Path("fromIndex") int fromIndex, @Query ("jwt") String token);
}
