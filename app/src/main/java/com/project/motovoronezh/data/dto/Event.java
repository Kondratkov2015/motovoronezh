package com.project.motovoronezh.data.dto;

public class Event {

    private int postId;
    private String title;
    private String message;
    private String poster;
    private long posted;

    public Event(int postId, String title, String message, String poster, long posted) {
        this.postId = postId;
        this.title = title;
        this.message = message;
        this.poster = poster;
        this.posted = posted;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public long getPosted() {
        return posted;
    }

    public void setPosted(long posted) {
        this.posted = posted;
    }
}
