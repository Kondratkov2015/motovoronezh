package com.project.motovoronezh.data.dto;

public class User {

    private int user_id;

    private String username;

    private String login;

    private String password;

    public User(int user_id, String login, String password, String username) {
        this.user_id = user_id;
        this.username = username;
        this.password = password;
        this.login = login;
    }

    public User(String name, int user_id) {
        this.username = name;
        this.login = name;
        this.user_id = user_id;
    }


    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.username = login;
    }

    public User(String username) {
        this.username = username;
    }

    public int getUser_id() {
        return this.user_id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return password;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
