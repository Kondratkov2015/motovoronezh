package com.project.motovoronezh.data.dto;

public class ShortPost {
    private int postId;
    private String title;
    private User user;
    private long data;


    public ShortPost(int id, String subject, String message, User user, long posted) {
        this.postId = id;
        this.title = subject;
        this.user = user;
        this.data = posted;
    }


    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public long getData() {
        return data;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setData(long data) {
        this.data = data;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
