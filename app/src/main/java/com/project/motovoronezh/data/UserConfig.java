package com.project.motovoronezh.data;

import android.content.Context;
import android.content.SharedPreferences;

public class UserConfig {

    private static final String USER_INFO = "User_Info";
    private static final String TOKEN = "Token";
    private static final String USER_ID = "User_Id";

    public static void saveUserInfo(String id, String token, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, token);
        editor.putString(USER_ID, id);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN, "");
    }

    public static String getUserId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, "");
    }

    public static void clearUserInfo(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(TOKEN);
        editor.remove(USER_ID);
        editor.apply();
    }
}
