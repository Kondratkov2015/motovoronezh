package com.project.motovoronezh.data.requests;

import android.util.Log;

import com.project.motovoronezh.data.RetrofitSingleton;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.services.NewsService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllNewsRequest {

    private List<Post> postList;

    public void getAllNews(String token, int fromIndex, Callable<Void> onSuccess) {
        NewsService retrofit = RetrofitSingleton.getInstance().create(NewsService.class);
        Call<Post[]> getAllNews = retrofit.getAllNews(fromIndex, token);
        getAllNews.enqueue(new Callback<Post[]>() {
            @Override
            public void onResponse(Call<Post[]> call, Response<Post[]> response) {
               if(response.isSuccessful()) {
                   Log.d("REQUEST", "GET All NEWS REQUEST SUCCESFULL");
                   postList = Arrays.asList(response.body());
                   try {
                       onSuccess.call();
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   return;
               }
                Log.e("REQUEST", "GET ALL NEWS request wasn't succesfull " + response.code());
            }

            @Override
            public void onFailure(Call<Post[]> call, Throwable t) {
                Log.e("REQUEST", "GET all USERs request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public List<Post> getPostList() {
        return postList;
    }
}
