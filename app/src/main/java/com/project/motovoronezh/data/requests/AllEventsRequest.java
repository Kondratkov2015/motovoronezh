package com.project.motovoronezh.data.requests;

import android.util.Log;

import com.project.motovoronezh.data.RetrofitSingleton;
import com.project.motovoronezh.data.dto.Event;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.services.EventsService;
import com.project.motovoronezh.data.services.NewsService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllEventsRequest {

    private List<Event> eventsList;

    public void getAllEvents(String token, Callable<Void> onSuccess) {
        EventsService retrofit = RetrofitSingleton.getInstance().create(EventsService.class);
        Call<Event[]> getAllEvents = retrofit.getAllNews(token);
        getAllEvents.enqueue(new Callback<Event[]>() {
            @Override
            public void onResponse(Call<Event[]> call, Response<Event[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "GET All NEWS REQUEST SUCCESFULL");
                    eventsList = Arrays.asList(response.body());
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "GET ALL NEWS request wasn't succesfull " + response.code());
            }

            @Override
            public void onFailure(Call<Event[]> call, Throwable t) {
                Log.e("REQUEST", "GET all USERs request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public List<Event> getEventsList() {
        return eventsList;
    }
}
