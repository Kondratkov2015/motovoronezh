package com.project.motovoronezh.data.services;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import static com.project.motovoronezh.data.services.Utils.loadImageFromWebOperations;

public class ImageLoader extends AsyncTask<String, Integer, Drawable> {

    @Override
    protected Drawable doInBackground(String... strings) {
        return loadImageFromWebOperations(strings[0]);
    }
}
