package com.project.motovoronezh.data.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.project.motovoronezh.R;
import com.project.motovoronezh.data.dto.ShortPost;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static java.lang.Math.min;

public class Utils {

    public static Drawable loadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }

    public static String textToHTMLText(String text) {
        return text.replace('[', '<')
                .replace(']', '>')
                .replace("\n", "<br>");
    }

    public static String convertDateToHumanReadable(long dateInLong) {
        Date date = new Date(dateInLong * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date);
    }

    public static boolean isVideoLink(String text) {
        return text.contains("[v]");
    }

    public static boolean isURLLink(String text) {
        return text.contains("[url=") && text.contains("[/url]");
    }

    public static boolean isHTTPLink(String text) {
        return text.startsWith("http://") || text.startsWith("https://");
    }

    public static String findImageURLFromText(String text) {
        int start = text.indexOf("[img]");
        int end = text.indexOf("[/img]");
        return start != -1 && end != -1 ? text.substring(start + 5, end) : null;
    }

    public static List<String> findAllImageURLFromText(String text) {
        List<String> URLs = new ArrayList<>();
        StringBuilder sb = new StringBuilder(text);
        int startIndex = sb.indexOf("[img]");
        int endIndex = sb.indexOf("[/img]");

        if (startIndex < 0) return null;

        while (startIndex >= 0) {
            endIndex = sb.indexOf("[/img]");
            URLs.add(sb.toString().substring(startIndex + 5, endIndex));
            sb.delete(startIndex, endIndex + 6);
            startIndex = sb.indexOf("[img]");
        }
        return URLs;
    }

    public static String deleteAllTagsFromText(String text) { //todo пока только img и некоторые разметочные. Сделать динамичным
        String newText = text;
        newText = deleteTagFromText(newText, "[img]", "[/img]");
        return newText;
    }

    public static String deleteAllHTMLTags(String text) {
        String newText = text;
        newText = deleteHTMLTagFromText(newText, "[b]", "[/b]");
        newText = deleteHTMLTagFromText(newText, "[s]", "[/s]");
        newText = deleteHTMLTagFromText(newText, "[i]", "[/i]");
        return newText;
    }

    public static String deleteTagFromText(String text, String tagOpen, String tagClose) {
        StringBuilder sb = new StringBuilder(text);
        int length = tagClose.length();
        int cutStartIndex = sb.indexOf(tagOpen);
        int cutEndIndex;
        while (cutStartIndex != -1) {
            cutEndIndex = sb.indexOf(tagClose) + length;
            sb.delete(cutStartIndex, cutEndIndex);
            cutStartIndex = sb.indexOf(tagOpen);
        }
        return sb.toString();
    }

    public static String deleteHTMLTagFromText(String text, String tagOpen, String tagClose) { //todo видимо не нужно
        StringBuilder sb = new StringBuilder(text);
        int tagOpenLength = tagOpen.length();
        int tagCloseLength = tagClose.length();

        int cutStart = sb.indexOf(tagOpen);

        while (cutStart >= 0) {
            sb.delete(cutStart, cutStart + tagOpenLength);
            cutStart = sb.indexOf(tagOpen);
        }

        cutStart = sb.indexOf(tagClose);

        while (cutStart >= 0) {
            sb.delete(cutStart, cutStart + tagCloseLength);
            cutStart = sb.indexOf(tagClose);
        }

        return sb.toString();
    }

    public static List<String> divideTextByImageTags(String text) {
        List<String> dividedText = new ArrayList<>();
        StringBuilder sb = new StringBuilder(text);

        int cutStartIndex = sb.indexOf("[img]");
        int cutEndIndex;

        if (cutStartIndex == -1) {
            dividedText.add(text);
            return dividedText;
        }

        while (cutStartIndex != -1) {
            String currentText = sb.toString();
            dividedText.add(currentText.substring(0, cutStartIndex)); //todo убирать здесь и все другие теги, не забыть про линки
            cutEndIndex = sb.indexOf("[/img]") + 6;
            sb.delete(0, cutEndIndex);
            cutStartIndex = sb.indexOf("[img]");
        }

        dividedText.add(sb.toString()); // последняя часть

        return dividedText;
    }

    public static List<String> divideTextByVideoTags(String text) {
        List<String> dividedText = new ArrayList<>();
        StringBuilder sb = new StringBuilder(text);

        int cutStartIndex = sb.indexOf("[v]");
        int cutEndIndex;

        if (cutStartIndex == -1) {
            dividedText.add(text);
            return dividedText;
        }

        while (cutStartIndex != -1) {
            String currentText = sb.toString();
            dividedText.add(currentText.substring(0, cutStartIndex));
            cutEndIndex = sb.indexOf("[/v]") + 4;
            dividedText.add(currentText.substring(cutStartIndex, cutEndIndex));
            sb.delete(0, cutEndIndex);
            cutStartIndex = sb.indexOf("[v]");
        }

        dividedText.add(sb.toString());
        return dividedText;
    }

    public static List<String> divideTextByURLTags(String text) {
        List<String> dividedText = new ArrayList<>();
        StringBuilder sb = new StringBuilder(text);

        int cutStartIndex = sb.indexOf("[url=");
        int cutEndIndex = sb.indexOf("[/url]");

        if (cutStartIndex == -1 || cutEndIndex == -1) {
            dividedText.add(text);
            return dividedText;
        }

        while (cutStartIndex != -1 && cutEndIndex != -1) {
            String currentText = sb.toString();
            cutEndIndex = sb.indexOf("[/url]") + 6;
            dividedText.add(currentText.substring(0, cutStartIndex)); //до тега
            dividedText.add(currentText.substring(cutStartIndex, cutEndIndex)); // тег с текстом внутри
            sb.delete(0, cutEndIndex);
            cutStartIndex = sb.indexOf("[url=");
        }

        dividedText.add(sb.toString()); // последняя часть
        return dividedText;
    }

    public static List<String> divideTextByHTTPLinks(String text) {
        List<String> dividedText = new ArrayList<>();
        StringBuilder sb = new StringBuilder(text);

        int cutStartIndex = sb.indexOf("http://") == -1 ?  sb.indexOf("https://") : sb.indexOf("http://");
        int cutEndIndex;

        if (cutStartIndex == -1 || isURLLink(text)) {
            dividedText.add(text);
            return dividedText;
        }

        while (cutStartIndex != -1) {
            dividedText.add(sb.toString().substring(0, cutStartIndex)); //до ссылки
            sb.delete(0, cutStartIndex);
            if (sb.indexOf(" ") != -1 && sb.indexOf("\n") != -1) {
                cutEndIndex = min(sb.indexOf(" "), sb.indexOf("\n"));
            } else
                cutEndIndex = sb.indexOf(" ") != -1 ? sb.indexOf(" ") : sb.indexOf("\n");
            if (cutEndIndex == -1) cutEndIndex = sb.length();
            dividedText.add(sb.toString().substring(0, cutEndIndex)); // ссылка
            sb.delete(0, cutEndIndex);
            cutStartIndex = sb.indexOf("http://") == -1 ?  sb.indexOf("https://") : sb.indexOf("http://");
        }

        dividedText.add(sb.toString()); // последняя часть
        return dividedText;
    }

    public static String getLinkFromVideoTag(String string) {
        int length = string.length();
        String substring = string.substring(3, length - 4);
        return substring.startsWith("http://") || substring.startsWith("https://") ?
                substring : "https://www.youtube.com/watch?v=" + substring;
    }

    public static String getLinkFromURLTag(String string) {
        StringBuilder sb = new StringBuilder(string);
        int startIndex = sb.lastIndexOf("[url=");
        sb.delete(0, startIndex + 5);
        int endIndex = sb.indexOf("]");
        return sb.toString().substring(0, endIndex);
    }

    public static LinearLayout createFullPostInfoLayout(String postTitle, String postMessage, int index, List<List<Drawable>> images, Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        TextView title = new TextView(context);
        title.setTypeface(null, Typeface.BOLD);
        title.setText(postTitle);
        title.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        title.setGravity(Gravity.LEFT);
        title.setPadding(10, 0, 0, 20);
        layout.addView(title);

        List<String> dividedByImgTagsMessage = divideTextByImageTags(postMessage);
        String firstText = dividedByImgTagsMessage.get(0);
        parseTextBlock(firstText, layout, context);

        int drawableIndex = index % 20;

        if (images != null && images.get(drawableIndex) != null && !images.get(drawableIndex).isEmpty()) {
            for (int i = 0; i < images.get(drawableIndex).size(); i++) {
                Drawable currentImage = images.get(drawableIndex).get(i);
                if (currentImage != null) {
                    ImageView imageView = new ImageView(context);
                    imageView.setAdjustViewBounds(true);
                    imageView.setImageDrawable(currentImage);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    layout.addView(imageView);
                    if (dividedByImgTagsMessage.get(i + 1).startsWith("[/url]")) {
                        int finalI = i;

                        Animation fadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fadein);
                        Animation fadeOutAnimation = AnimationUtils.loadAnimation(context, R.anim.fadeout);
                        Animation.AnimationListener animationFadeInListener = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                imageView.startAnimation(fadeOutAnimation);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        };
                        Animation.AnimationListener animationFadeOutListener = new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                imageView.startAnimation(fadeInAnimation);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        };
                        fadeInAnimation.setAnimationListener(animationFadeInListener);
                        fadeOutAnimation.setAnimationListener(animationFadeOutListener);
                        imageView.startAnimation(fadeOutAnimation);

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String link = getLinkFromURLTag(dividedByImgTagsMessage.get(finalI));
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                                browserIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(browserIntent);
                            }
                        });
                    }
                }

                parseTextBlock(dividedByImgTagsMessage.get(i + 1), layout, context);

            }
        }
        return layout;
    }

    public static LinearLayout createShortPostInfoLayout(List<ShortPost> posts, int index, Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        ShortPost post = posts.get(index);

        TextView title = new TextView(context);
        title.setTypeface(null, Typeface.BOLD);
        title.setText(post.getTitle() + "\n");
        title.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        title.setGravity(Gravity.LEFT);
        title.setPadding(10, 0, 0, 0);
        layout.addView(title);

        LinearLayout postInfoLayout = new LinearLayout(context);
        postInfoLayout.setOrientation(LinearLayout.HORIZONTAL);
        postInfoLayout.setWeightSum(1);
        postInfoLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        TextView posterName = new TextView(context);
        posterName.setText(post.getUser().getUsername());
        posterName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        posterName.setPadding(10, 0, 0, 0);
        posterName.setGravity(Gravity.LEFT);
        postInfoLayout.addView(posterName);

        String stringDate = convertDateToHumanReadable(post.getData());

        TextView postDate = new TextView(context);
        postDate.setText(stringDate);
        postDate.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        postDate.setPadding(0, 0, 10, 0);
        postDate.setGravity(Gravity.RIGHT);
        postInfoLayout.addView(postDate);

        layout.addView(postInfoLayout);

        //layout.setBackground(ContextCompat.getDrawable(context, R.drawable.text_view_border));
        return layout;
    }

    public static void parseTextBlock(String text, LinearLayout layout, Context context) {
        for (String dividedByVideoTextPart : divideTextByVideoTags(text)) {
            for (String dividedByURLTextPart : divideTextByURLTags(dividedByVideoTextPart)) {
                for (String textPart : divideTextByHTTPLinks(dividedByURLTextPart)) {
                    if (!textPart.isEmpty() && !textPart.equals(".")) {
                        TextView textPartTV = new TextView(context);
                        if (isVideoLink(textPart)) {
                            String finalTextPart = textPart;
                            textPartTV.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getLinkFromVideoTag(finalTextPart)));
                                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(browserIntent);
                                }
                            });
                            textPartTV.setTextColor(Color.parseColor("#0E67ED"));
                        }
                        if (isURLLink(textPart)) {
                            String finalTextPart = textPart;
                            textPartTV.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getLinkFromURLTag(finalTextPart)));
                                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(browserIntent);
                                }
                            });
                            textPartTV.setTextColor(Color.parseColor("#0E67ED"));
                        }
                        if (isHTTPLink(textPart)) {
                            String finalTextPart = textPart;
                            textPartTV.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalTextPart));
                                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(browserIntent);
                                }
                            });
                            textPartTV.setTextColor(Color.parseColor("#0E67ED"));
                        }

                        textPart = textToHTMLText(textPart);
                        textPartTV.setText(Html.fromHtml(textPart));
                        textPartTV.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        textPartTV.setGravity(Gravity.LEFT);
                        textPartTV.setPadding(10, 0, 0, 0);
                        layout.addView(textPartTV);
                    }
                }
            }
        }
    }
}
