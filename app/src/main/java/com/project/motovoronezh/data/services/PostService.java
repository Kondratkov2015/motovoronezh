package com.project.motovoronezh.data.services;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PostService {

    @GET("/get_message_from_post/{postId}")
    Call<JsonObject> getPostMessage(@Path("postId") int postId, @Query("jwt") String token);

}
