package com.project.motovoronezh.data.dto;

public class Post {

    private int id;
    private String title;
    private String message;
    private User user;
    private long data;

    public Post(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public Post(String subject, String message, User user, long posted) {
        this.title = subject;
        this.message = message;
        this.data = posted;
        this.user = user;
    }

    public Post(int id, String subject, String message, User user, long posted) {
        this.id = id;
        this.title = subject;
        this.message = message;
        this.data = posted;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public long getData() {
        return data;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setData(long data) {
        this.data = data;
    }
}
