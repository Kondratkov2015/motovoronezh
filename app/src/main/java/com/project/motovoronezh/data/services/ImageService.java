package com.project.motovoronezh.data.services;

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ImageService {

    @Multipart()
    @POST("/1/upload")
    Call<JsonObject> uploadImage(@Query("key") String key, @Part() MultipartBody.Part file );

}
