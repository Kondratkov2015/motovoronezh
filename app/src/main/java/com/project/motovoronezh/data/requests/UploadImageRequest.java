package com.project.motovoronezh.data.requests;

import com.google.gson.JsonObject;
import com.project.motovoronezh.data.RetrofitIMGBB;
import com.project.motovoronezh.data.services.ImageService;
import java.util.concurrent.Callable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageRequest {

    private String photoURL;

    public void uploadImage(MultipartBody.Part body, Callable<Void> onSuccess) {

        ImageService retrofit = RetrofitIMGBB.getInstance().create(ImageService.class);
        Call<JsonObject> call = retrofit.uploadImage(RetrofitIMGBB.KEY_API, body);
        call.enqueue(new Callback<JsonObject>() {


            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    photoURL = response.body().get("data").getAsJsonObject().get("url").getAsString();
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("I send photo");
                }
                else
                    System.out.println("Can't send one of photos");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println("on failure");
            }
        });
    }

    public String getPhotoURL() {
        return photoURL;
    }
}
