package com.project.motovoronezh.data.requests;

import android.util.Log;

import com.project.motovoronezh.data.RetrofitSingleton;
import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.dto.ShortPost;
import com.project.motovoronezh.data.services.DalnoboyService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllDalnoboyRequest {

    private List<ShortPost> dalnoboysPostList;

    public void getAllDalnoboy(String token, int fromIndex, Callable<Void> onSuccess) {
        DalnoboyService retrofit = RetrofitSingleton.getInstance().create(DalnoboyService.class);
        Call<ShortPost[]> getAllShortDalnoboys = retrofit.getAllDalnoboy(fromIndex, token);
        getAllShortDalnoboys.enqueue(new Callback<ShortPost[]>() {
            @Override
            public void onResponse(Call<ShortPost[]> call, Response<ShortPost[]> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "GET All NEWS REQUEST SUCCESFULL");
                    dalnoboysPostList = Arrays.asList(response.body());
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "GET ALL NEWS request wasn't succesfull " + response.code());
            }

            @Override
            public void onFailure(Call<ShortPost[]> call, Throwable t) {
                Log.e("REQUEST", "GET all USERs request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public List<ShortPost> getDalnoboysPostList() {
        return dalnoboysPostList;
    }
}
