package com.project.motovoronezh.data.services;

import com.project.motovoronezh.data.dto.LoginPostResult;
import com.project.motovoronezh.data.dto.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("/cgi-bin/app.py/login")
    Call<LoginPostResult> loginUser(@Body User body);

}
