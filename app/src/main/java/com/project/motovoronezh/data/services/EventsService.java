package com.project.motovoronezh.data.services;

import com.project.motovoronezh.data.dto.Event;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EventsService {

    @GET("/get_all_events")
    Call<Event[]> getAllNews(@Query("jwt") String token);
}
