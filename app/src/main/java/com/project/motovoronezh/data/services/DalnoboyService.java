package com.project.motovoronezh.data.services;

import com.project.motovoronezh.data.dto.Post;
import com.project.motovoronezh.data.dto.ShortPost;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DalnoboyService {

    @GET("get_all_short_posts_dalnoboy/{fromIndex}")
    Call<ShortPost[]> getAllDalnoboy(@Path("fromIndex") int fromIndex, @Query("jwt") String token);
}
