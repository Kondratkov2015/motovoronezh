package com.project.motovoronezh.data.requests;

import android.util.Log;

import com.google.gson.JsonObject;
import com.project.motovoronezh.data.RetrofitSingleton;
import com.project.motovoronezh.data.services.PostService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostRequests {

    private String message;

    public void getPostMessage(String token, int postId, Callable<Void> onSuccess) {
        PostService retrofit = RetrofitSingleton.getInstance().create(PostService.class);
        Call<JsonObject> getPostMessage = retrofit.getPostMessage(postId, token);
        getPostMessage.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    Log.d("REQUEST", "GET POST MESSAGE REQUEST SUCCESFULL");
                    message = response.body().get("message").getAsString();
                    try {
                        onSuccess.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Log.e("REQUEST", "GET POST MESSAGE request wasn't succesfull " + response.code());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("REQUEST", "GET POST MESSAGE request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }

    public String getMessage() {
        return message;
    }
}
