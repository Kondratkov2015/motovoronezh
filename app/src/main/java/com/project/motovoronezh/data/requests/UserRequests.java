package com.project.motovoronezh.data.requests;

import android.content.Context;
import android.util.Log;

import com.project.motovoronezh.data.RetrofitSingleton;
import com.project.motovoronezh.data.UserConfig;
import com.project.motovoronezh.data.dto.LoginPostResult;
import com.project.motovoronezh.data.dto.User;
import com.project.motovoronezh.data.services.UserService;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRequests {

    public static void login(String login, String password, Context context, Callable<Void> onSucces, Callable<Void> onFailure) {
        UserService retrofit = RetrofitSingleton.getInstance().create(UserService.class);
        Call<LoginPostResult> loginCall = retrofit.loginUser(new User(login, password));
        final LoginPostResult[] result = new LoginPostResult[1];
        loginCall.enqueue(new Callback<LoginPostResult>() {
            @Override
            public void onResponse(Call<LoginPostResult> call, Response<LoginPostResult> response) {
                if(response.isSuccessful()) {
                    Log.d("REQUEST", "LOGIN REQUEST SUCCESFULL\nToken: " + response.body().getToken()
                            + "\nId: " + response.body().getId());
                    UserConfig.saveUserInfo(response.body().getId(), response.body().getToken(), context);
                    try {
                        onSucces.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return;
                }
                Log.e("REQUEST", "Login request wasn't succesfull");
                try {
                    onFailure.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<LoginPostResult> call, Throwable t) {
                Log.e("REQUEST", "Login request wasn't succesfull");
                t.printStackTrace();
            }
        });
    }
}
