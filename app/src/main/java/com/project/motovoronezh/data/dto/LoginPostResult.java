package com.project.motovoronezh.data.dto;

public class LoginPostResult {

    private String id;

    private String token;


    public LoginPostResult(String id, String token) {
        this.id = id;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }
}
